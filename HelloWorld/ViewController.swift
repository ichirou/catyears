//
//  ViewController.swift
//  HelloWorld
//
//  Created by Richmond Ko on 29/08/2016.
//  Copyright © 2016 Richmond Ko. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet var catYearsLabel: UILabel!
    @IBOutlet var catAgeInput: UITextField!
    
    @IBAction func submitButton(_ sender: AnyObject) {
        
        let ageInCatYears = Int(catAgeInput.text!)! * 7
        
        catYearsLabel.text = String(ageInCatYears)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
